#!/bin/bash

if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
	export QT_QPA_PLATFORM=wayland
	cat > ~/.config/electron-flags.conf <<- EOF
		--enable-features=WaylandWindowDecorations,UseOzonePlatform
		--ozone-platform=wayland
	EOF

	cat > ~/.config/brave-flags.conf <<- EOF
		--ozone-platform-hint=auto
		--ignore-gpu-blocklist
		--enable-zero-copy
		--enable-features=VaapiVideoDecodeLinuxGL,VaapiIgnoreDriverChecks,VaapiVideoDecoder,VaapiVideoEncoder
		--disable-features=UseChromeOSDirectVideoDecoder
		--enable-gpu-rasterization
		--enable-drdc
		--canvas-oop-rasterization
		--enable-raw-draw
		--usr-gpu-scheduler-dfs
		--enable-accelerated-video-decode
		--use-gl=angle
	EOF
else
	export QT_QPA_PLATFORM=xcb
	echo "" > ~/.config/electron-flags.conf

	cat > ~/.config/brave-flags.conf <<- EOF
		--ignore-gpu-blocklist
		--enable-zero-copy
		--enable-features=VaapiVideoDecodeLinuxGL,VaapiIgnoreDriverChecks,VaapiVideoDecoder,VaapiVideoEncoder
		--disable-features=UseChromeOSDirectVideoDecoder
		--enable-gpu-rasterization
		--enable-drdc
		--canvas-oop-rasterization
		--enable-raw-draw
		--usr-gpu-scheduler-dfs
		--enable-accelerated-video-decode
	EOF
fi
